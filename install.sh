#!/bin/bash

# Afegir repos extra ubuntu, no disponibles per defecte a la 18.04.1, sí a la 18.04
sudo add-apt-repository universe
sudo add-apt-repository restricted
sudo add-apt-repository multiverse

# Afegir repositori debian de Linkat
wget http://download-linkat.xtec.cat/distribution/linkat-edu-18.04/linkat-repo-18.04.list
sudo mv linkat-repo-18.04.list /etc/apt/sources.list.d/

# Afegir key repositori anterior
wget http://download-linkat.xtec.cat/distribution/linkat-edu-18.04/pubkey-linkat.gpg
sudo apt-key add pubkey-linkat.gpg


# Actualitzar via APT i instal·lar dependències del Servidor de comunicacions
sudo apt update
sudo apt install -y ntp squid isc-dhcp-server bind9 squidguard python3-pip libjson-xs-perl libcurses-ui-perl webmin

# Instal·lació dependències Python
sudo pip3 install python-iptables


# Descàrrega paqueteria Servidor Comunicacions
git clone https://gitlab.com/linkatedu/linkat-servidor-comunicacions.git
git clone https://gitlab.com/linkatedu/linkat-lustitia.git
git clone https://gitlab.com/linkatedu/wbm-squidguard.git
git clone https://gitlab.com/linkatedu/wbm-lustitia.git

# Instal·lació paqueteria Servidor Comunicacions per a Webmin
sudo cp -r wbm-squidguard/ /etc/webmin/squidguard
sudo cp -r wbm-squidguard/ /usr/share/webmin/squidguard
sudo cp -r wbm-lustitia/ /etc/webmin/lustitia
sudo cp -r wbm-lustitia/ /usr/share/webmin/lustitia


# Instal·lació paqueteria Servidor Comunicacions per a Webmin
mkdir $HOME/backupDIR # backup temporal pel desenvolupament
sudo rsync -a --exclude=README.md --exclude=.git --backup-dir=$HOME/backupDIR linkat-lustitia/ /
sudo rsync -a --exclude=README.md --exclude=.git --backup-dir=$HOME/backupDIR linkat-servidor-comunicacions/ /


# Comandes pendents de gestionar
# sudo chmod +x /etc/init.d/lustitia
sudo locale-gen es_ES.UTF-8 # Per evitar warnings Perl.
sudo sh -c 'echo "redirect_program /usr/bin/squidGuard -c /etc/squidguard/squidGuard.conf" >> /etc/squid/squid.conf'

