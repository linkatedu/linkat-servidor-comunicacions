sh run | exc password|secret|community|snmp-server host|tacacs-$c password|secret|community|snmp-server host|tacacs-s         erver key
Building configuration...

Current configuration : 12827 bytes
!
! Last configuration change at 14:16:28 GMT+1 Tue Dec 5 2017
! NVRAM config last updated at 14:16:29 GMT+1 Tue Dec 5 2017
!
version 15.0
no service pad
service tcp-keepalives-in
service tcp-keepalives-out
service timestamps debug datetime msec localtime show-timezone
service timestamps log datetime msec localtime show-timezone
!
hostname R0SC0_E606870
!
boot-start-marker
boot-end-marker
!
logging buffered 512000
!
aaa new-model
!
!
aaa authentication login default local
aaa authentication login CONSOLA line
aaa authentication enable default enable
aaa authorization exec default local 
!
!
!
!
!
!
aaa session-id common
clock timezone GMT+1 1 0
clock summer-time GMT+1 recurring last Sun Mar 2:00 last Sun Oct 3:00
switch 1 provision ws-c3750x-24
system mtu routing 1500
no ip source-route
ip routing
ip dhcp relay information trust-all
ip dhcp excluded-address 10.245.127.193 10.245.127.212
ip dhcp excluded-address 192.168.0.1 192.168.0.255
ip dhcp excluded-address 192.168.32.1 192.168.32.255
ip dhcp excluded-address 192.168.56.1 192.168.56.127
ip dhcp excluded-address 10.245.127.254
!
ip dhcp pool Educativa
 network 192.168.0.0 255.255.252.0
 default-router 192.168.0.1 
 dns-server 213.176.161.16 213.176.161.18 
!
ip dhcp pool Wifi_Educativa
 network 192.168.16.0 255.255.252.0
 default-router 192.168.16.1 
 dns-server 213.176.161.16 213.176.161.18 
!
ip dhcp pool Educativa_Aillada
 network 192.168.32.0 255.255.252.0
 default-router 192.168.32.1 
 dns-server 213.176.161.16 213.176.161.18 
!
ip dhcp pool Gestio_LAN_C6
 network 10.245.127.192 255.255.255.192
 default-router 10.245.127.193 
 option 43 hex f104.0a01.0604
!
ip dhcp pool eduroam
 network 192.168.48.0 255.255.255.0
 default-router 192.168.48.1 
 dns-server 213.176.161.16 213.176.161.18 
!
ip dhcp pool Edifici
 network 192.168.56.0 255.255.255.0
 default-router 192.168.56.1 
 dns-server 213.176.161.16 213.176.161.18 
!
!
ip dhcp snooping vlan 20-40,50,60,70-74,81-82,86
no ip dhcp snooping information option
ip dhcp snooping
no ip domain-lookup
ip domain-name c6.tsystems.es
vtp mode off
!
!
!
!
!
!
!
spanning-tree mode rapid-pvst
spanning-tree portfast bpduguard default
spanning-tree extend system-id
spanning-tree vlan 1-100 priority 4096
!
!
!
!
!
!
network-policy profile 1
 voice vlan 50
!
port-channel load-balance src-dst-ip
!
!
!
!
vlan internal allocation policy ascending
!
vlan 10
 name Transport
!
vlan 20
 name Educativa
!
vlan 30
 name Educativa_Aillada
!
vlan 36
 name Administrativa
!
vlan 40
 name Servidors
!
vlan 50
 name Telefonia
!
vlan 60
 name Videoconferencia
!
vlan 70
 name Eduroam
!
vlan 71
 name Wifi_Educativa
!
vlan 81
 name Gestio_electronica
!
vlan 82
 name Impressio
!
vlan 86
 name Edifici
!
ip ssh version 2
lldp run
!
! 
!
!
!
!
!
!
!
!
interface Port-channel1
 description SWC6_E610841_E632336
 switchport trunk encapsulation dot1q
 switchport mode trunk
 storm-control broadcast level 20.00
 storm-control action trap
 ip dhcp snooping trust
!
interface Port-channel2
 description SWC6_E632337
 switchport trunk encapsulation dot1q
 switchport mode trunk
 storm-control broadcast level 20.00
 storm-control action trap
 ip dhcp snooping trust
!
interface Port-channel3
 description SWC6_E632424
 switchport trunk encapsulation dot1q
 switchport mode trunk
 storm-control broadcast level 20.00
 storm-control action trap
 ip dhcp snooping trust
!
interface FastEthernet0
 no ip address
 no ip route-cache
 shutdown
!
interface GigabitEthernet1/0/1
!
interface GigabitEthernet1/0/2
 description ROU_1
 switchport trunk encapsulation dot1q
 switchport trunk allowed vlan 10,50,60
 switchport mode trunk
 ip dhcp snooping limit rate 25
 ip dhcp snooping trust
!
interface GigabitEthernet1/0/3
 description ROU_routerfutur
 switchport trunk encapsulation dot1q
 switchport trunk allowed vlan 10,50,60
 switchport mode trunk
 shutdown
!
interface GigabitEthernet1/0/4
 description Bluebox_Produccio
 switchport access vlan 40
 switchport mode access
 switchport port-security maximum 3
 switchport port-security
!
interface GigabitEthernet1/0/5
 description Bluebox_Gestio
 switchport access vlan 81
 switchport mode access
 switchport port-security maximum 3
 switchport port-security
!
interface GigabitEthernet1/0/6
 description Servidor_Centre
 switchport access vlan 20
 switchport mode access
 switchport port-security maximum 3
 switchport port-security
 storm-control broadcast level 20.00
 storm-control action shutdown
 storm-control action trap
!
interface GigabitEthernet1/0/7
 description Servidor_Futurs
 switchport access vlan 40
 switchport mode access
 switchport port-security maximum 3
 switchport port-security
 shutdown
 storm-control broadcast level 20.00
 storm-control action shutdown
 storm-control action trap
!
interface GigabitEthernet1/0/8
 description Servidor_Futurs
 switchport access vlan 40
 switchport mode access
 switchport port-security maximum 3
 switchport port-security
 shutdown
 storm-control broadcast level 20.00
 storm-control action shutdown
 storm-control action trap
!
interface GigabitEthernet1/0/9
 description SWC6_switchfutur
 switchport trunk encapsulation dot1q
 switchport mode trunk
 shutdown
!
interface GigabitEthernet1/0/10
 description SWC6_switchfutur
 switchport trunk encapsulation dot1q
 switchport mode trunk
 shutdown
!
interface GigabitEthernet1/0/11
 description SWC6_switchfutur
 switchport trunk encapsulation dot1q
 switchport mode trunk
 shutdown
!
interface GigabitEthernet1/0/12
 description SWC6_switchfutur
 switchport trunk encapsulation dot1q
 switchport mode trunk
 shutdown
!
interface GigabitEthernet1/0/13
 description SWC6_switchfutur
 switchport trunk encapsulation dot1q
 switchport mode trunk
 shutdown
!
interface GigabitEthernet1/0/14
 description SWC6_switchfutur
 switchport trunk encapsulation dot1q
 switchport mode trunk
 shutdown
!
interface GigabitEthernet1/0/15
 description SWC6_switchfutur
 switchport trunk encapsulation dot1q
 switchport mode trunk
 shutdown
!
interface GigabitEthernet1/0/16
 description SWC6_switchfutur
 switchport trunk encapsulation dot1q
 switchport mode trunk
 shutdown
!
interface GigabitEthernet1/0/17
 description SWC6_switchfutur
 switchport trunk encapsulation dot1q
 switchport mode trunk
 shutdown
!
interface GigabitEthernet1/0/18
 description SWC6_switchfutur
 switchport trunk encapsulation dot1q
 switchport mode trunk
 shutdown
!
interface GigabitEthernet1/0/19
 description SWC6_E632424
 switchport trunk encapsulation dot1q
 switchport mode trunk
 storm-control broadcast level 20.00
 storm-control action trap
 channel-protocol lacp
 channel-group 3 mode active
 ip dhcp snooping trust
!
interface GigabitEthernet1/0/20
 description SWC6_E632424
 switchport trunk encapsulation dot1q
 switchport mode trunk
 storm-control broadcast level 20.00
 storm-control action trap
 channel-protocol lacp
 channel-group 3 mode active
 ip dhcp snooping trust
!
interface GigabitEthernet1/0/21
 description SWC6_E632337
 switchport trunk encapsulation dot1q
 switchport mode trunk
 storm-control broadcast level 20.00
 storm-control action trap
 channel-protocol lacp
 channel-group 2 mode active
 ip dhcp snooping trust
!
interface GigabitEthernet1/0/22
 description SWC6_E632337
 switchport trunk encapsulation dot1q
 switchport mode trunk
 storm-control broadcast level 20.00
 storm-control action trap
 channel-protocol lacp
 channel-group 2 mode active
 ip dhcp snooping trust
!
interface GigabitEthernet1/0/23
 description SWC6_E610841_E632336
 switchport trunk encapsulation dot1q
 switchport mode trunk
 storm-control broadcast level 20.00
 storm-control action trap
 channel-protocol lacp
 channel-group 1 mode active
 ip dhcp snooping trust
!
interface GigabitEthernet1/0/24
 description SWC6_E610841_E632336
 switchport trunk encapsulation dot1q
 switchport mode trunk
 storm-control broadcast level 20.00
 storm-control action trap
 channel-protocol lacp
 channel-group 1 mode active
 ip dhcp snooping trust
!
interface GigabitEthernet1/1/1
!
interface GigabitEthernet1/1/2
!
interface GigabitEthernet1/1/3
!
interface GigabitEthernet1/1/4
!
interface TenGigabitEthernet1/1/1
!
interface TenGigabitEthernet1/1/2
!
interface Vlan1
 no ip address
 shutdown
!
interface Vlan10
 description VLC6_Transport
 ip address 10.242.43.228 255.255.255.240
!
interface Vlan20
 description VLC6_Educativa
 ip address 192.168.0.1 255.255.252.0
 ip access-group 121 in
!
interface Vlan30
 description VLC6_Educativa_Aillada
 ip address 192.168.32.1 255.255.252.0
 ip access-group 121 in
!
interface Vlan36
 description VLC6_Administrativa
 ip address 10.205.201.1 255.255.255.128
 ip access-group 120 in
 ip helper-address 10.1.0.11
!
interface Vlan40
 description VLC6_Servidors
 ip address 10.241.149.217 255.255.255.248
!
interface Vlan70
 description VLC6_Eduroam
 ip address 192.168.48.1 255.255.255.0
 ip access-group 121 in
!
interface Vlan71
 description VLC6_Wifi_Educativa
 ip address 192.168.16.1 255.255.252.0
 ip access-group 121 in
!
interface Vlan81
 description VLC6_Gestio_Electronica
 ip address 10.245.127.193 255.255.255.192
 ip access-group 69 out
!
interface Vlan82
 description VLC6_Impressores
 ip address 10.241.21.158 255.255.255.248
!
interface Vlan86
 description VLC6_Edifici
 ip address 192.168.56.1 255.255.255.0
 ip access-group 121 in
!
ip forward-protocol udp discard
ip forward-protocol udp echo
ip forward-protocol udp 5246
ip forward-protocol udp 12223
no ip http server
no ip http secure-server
!
ip route 0.0.0.0 0.0.0.0 10.242.43.225
!
ip sla enable reaction-alerts
logging trap debugging
logging facility local0
logging host 198.18.246.245
access-list 69 permit 10.35.8.55
access-list 69 permit 10.35.8.57
access-list 69 permit 10.35.8.56
access-list 69 permit 192.168.136.254
access-list 69 permit 10.42.1.142
access-list 69 permit 10.244.55.64 0.0.0.31
access-list 69 permit 10.1.6.0 0.0.0.31
access-list 69 permit 10.1.10.128 0.0.0.127
access-list 69 permit 10.2.10.128 0.0.0.127
access-list 69 permit 198.18.246.240 0.0.0.15
access-list 69 permit 10.96.52.0 0.0.0.31
access-list 69 permit 198.18.206.0 0.0.0.255
access-list 69 permit 10.29.11.0 0.0.0.255
access-list 69 permit 10.55.6.0 0.0.1.255
access-list 69 permit 10.55.8.0 0.0.1.255
access-list 69 permit 198.18.102.0 0.0.1.255
access-list 69 permit 198.19.102.0 0.0.1.255
access-list 69 permit 10.155.22.0 0.0.0.255
access-list 120 permit ip any 192.168.0.0 0.0.0.255
access-list 120 deny   ip any 192.168.0.0 0.0.63.255
access-list 120 permit ip any any
access-list 121 permit icmp 192.168.0.0 0.0.0.255 any
access-list 121 deny   ip 192.168.32.0 0.0.31.255 10.241.0.0 0.0.255.255
access-list 121 permit tcp any 10.241.128.0 0.0.127.255 eq 9191
access-list 121 permit tcp any 10.241.128.0 0.0.127.255 eq 9192
access-list 121 permit tcp any 10.241.128.0 0.0.127.255 eq 9193
access-list 121 permit tcp any 10.241.128.0 0.0.127.255 eq 445
access-list 121 permit ip host 192.168.0.100 10.204.0.0 0.3.255.255
access-list 121 deny   ip any 10.204.0.0 0.3.255.255
access-list 121 deny   ip any 10.241.0.0 0.0.255.255
access-list 121 permit ip 192.168.0.0 0.0.31.255 192.168.0.0 0.0.31.255
access-list 121 deny   ip any 192.168.0.0 0.0.63.255
access-list 121 permit ip any any
!
snmp-server location "3448 - 08035453 - Escola Tomas Moro"
snmp-server contact "GESTIO LAN C6" HLD v1.0
snmp-server system-shutdown
snmp-server enable traps fru-ctrl
snmp-server enable traps cpu threshold
snmp-server enable traps storm-control trap-rate 5
snmp-server enable traps bridge newroot topologychange
snmp-server enable traps stpx inconsistency root-inconsistency loop-inconsistency
snmp-server enable traps vtp
snmp-server enable traps flash insertion removal
snmp-server enable traps errdisable
!
!
!
banner login ^C
AUTORITZACIO D'ACCES A L'ELECTRONICA DE XARXA
Voste esta accedint a un dispositiu propietat del Departament d'Ensenyament de la Generalitat de Catalunya. 
Tots els accessos estan monitorats i registrats. Qualsevol acces us no autoritzat del mateix sera motiu de
sancio o de les penalitats corresponents segons la legislacio vigent aplicable.
^C
banner motd ^C3448_ENS_08035453 - Escola Tomas Moro_E606870^C
!
line con 0
 privilege level 15
 logging synchronous
 login authentication CONSOLA
line vty 0 4
 logging synchronous
 transport input ssh
line vty 5 15
 logging synchronous
 transport input ssh
!
ntp server 192.168.90.6
ntp server 198.18.246.242 prefer
end

R0SC0_E606870#
