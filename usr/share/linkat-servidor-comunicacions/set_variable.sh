#!/bin/bash
# Modificacions a /etc/sysconfig/network/config
S_FILE=$1
S_KEY=$2
S_VALUE=$3
S_WRAPPER=$4

sed -i "s/\(^ *$S_KEY *= *\).*/\1$S_WRAPPER$S_VALUE$S_WRAPPER/" $S_FILE
