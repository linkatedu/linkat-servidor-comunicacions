#/usr/bin/env perl

use strict;
use warnings;
use JSON::XS;
use Curses::UI;
use Curses::UI::Common;
use IO::Select;



my @interfaces_list = `ip link sh | grep ^[0-9] | grep -v " lo" | cut -d":" -f 2 | tr -d " "`;
chomp @interfaces_list;

my @interfaces_names = @interfaces_list;
my @interfaces_values = [@interfaces_list];
my %interfaces_labels;
my $interfaces_labels;

foreach my $i (0 .. $#interfaces_list) {
   $interfaces_labels{$i+1} = $interfaces_list[$i];
}



my %CONF = (
	"working_directory","/usr/share/linkat-servidor-comunicacions",
	"config_directory","/etc/linkat-servidor-comunicacions",
	"config_file",		"config.json",
	"lustitia_file","/etc/lustitia.conf",
	"dhcp_server_file","/etc/default/isc-dhcp-server",
	"rosco_config_file","rosco.json",
	"rosco_load_binary_file","rosco_load.py",
	"rosco_apply_binary_file","rosco_apply.py",
	"set_variable",		"set_variable.sh",

	"bg", 				"blue",
	"fg", 				"white",
	"bfg", 			"white",

	"buttons_width",	"40",
	"window_title",		"Linkat: Configuració del servidor de comunicacion",
);


# UI elements
my $menu;
my $win;
my $winWidth;
my $winHeigth;
my @buttons = (
    {
		-label => "Configuració interfície WAN",
		-onpress => \&view_interfaces
	},
    {
		-label => "Configuració LAN - ROSCO",
		-onpress => \&view_lan_config
	},
    {
		-label => "Configuració Routers de sortida",
		-onpress => \&view_routers
	}
);


# CONFIG DATA
open( my $fh, '<', $CONF{config_directory} . "/" . $CONF{config_file});
my $json_text   = do { local $/; <$fh>};
close($fh);

my $configuration = JSON::XS->new->utf8->decode( $json_text );
my $typology = $configuration->{wan};

# my $cui = new Curses::UI( -color_support => 1, -debug => 1);
my $cui = new Curses::UI( -color_support => 1);


sub window_create {
	$win = $cui->add(
        'win', 'Window',
        -border     => 1,
        -bfg        => $CONF{bfg},
        -bbg        => $CONF{bg},
        -bg         => $CONF{bg},
        -fg         => $CONF{fg},
        -vscrollbar => "right"
	);

    $winWidth = $win->width();
    $winHeigth = $win->height();


	my $title = $win->add(
		'title', 'Label',
		-text	=> $CONF{window_title},
		-bold   => 1,
		-y      => 0,
		-x      => 1,
	);
}

sub view_menu_home {
	$cui->delete('win');
	window_create();

	my $menu_x = ($win->width() - $CONF{buttons_width}) / 2;
	my $menu_y = ($win->height() - (4 + scalar @buttons)) / 2;

	$menu = $win->add(
		'menu', 'Buttonbox',
		-buttons   			=> [@buttons],
		-vertical   		=> 1,
		-buttonalignment	=> 'middle',
		-title      		=> 'Opcions de configuració',
		-border     		=> 1,
		-bbg  				=> $CONF{bg},
		-tbg 				=> $CONF{fg},
		-tfg 				=> $CONF{bg},
		-ipad 				=> 1,
		-width      		=> $CONF{buttons_width},
		-x  				=> $menu_x,
		-y  				=> $menu_y,
	);
	my $menuWidth = $menu->width();
	my $menuHeight = $menu->height();

	my $bpos = add_button("APLICAR CONFIGURACIÓ", "homeApply", \&apply_changes, $menu_x, $menu_y + $menuHeight , 0);
	add_button("SORTIR", "homeExit", \&exit, $bpos->{endX}+1, $menu_y + $menuHeight , 0);


	$menu->focus();
}

sub view_lan_config {
	$cui->delete('win');
	window_create();

    $win->add(
        "LAN",
        "Label",
        -text => "VLAN's > Configuració obtinguda de fitxer ROSCO",
		-bold   => 1,
		-y      => 3,
		-x      => 1,
    );

    my $co = $Curses::UI::color_object;


    open( my $fh, '<', $CONF{config_directory} . "/" . $CONF{rosco_config_file});
    my $rosco_text   = do { local $/; <$fh>};
    close($fh);

	add_button("CARREGAR DES DE FITXER ROSCO", "loadRoscoBrowser", \&view_lan_rosco_filebrowser, 2, 5, 0);

    my $textviewer = $win->add(
        'mytextviewer', 'TextViewer',
        -text => $rosco_text,
		-y => 7,
		-x => 2,
        -width  => $winWidth-10,
        -height => $winHeigth-15,
        -border => 1,
        -vscrollbar => "true",
		-bfg  	=> $CONF{bfg},
		-bbg  	=> $CONF{bg},
		-bg 	=> $CONF{bg},
		-fg 	=> $CONF{fg}

    );

	my $btsave = add_button("TORNAR", "vlanApply", \&view_menu_home, $winWidth - 28, $winHeigth - 5, 0);

    $textviewer->focus();
}

sub view_lan_rosco_filebrowser {


    my $yes = $cui->dialog(
        -message => "A continuació es procedirà a carrergar el fitxer ROSCO per obtenir-ne la configuració. Aquesta operació substitueix l'actual configuració. Estàs segur de continuar? Recorda que caldrà aplicar els canvis per fer-los efectius",
        -buttons => [
            { -label => "Sí", -value => 1 },
            { -label => "No", -value => 0 }
        ],
        -bfg     => $CONF{bfg},
        -bbg     => $CONF{bg},
        -bg      => $CONF{bg},
    );

    if (!$yes) {
        view_lan_config();
        return;
    }


	$cui->delete('win');
	window_create();

    $win->add(
        "LAN",
        "Label",
        -text => "VLAN's > Configuració obtinguda de fitxer ROSCO",
		-bold   => 1,
		-y      => 3,
		-x      => 1,
    );

    my $file = $cui->filebrowser(-title => "Selecciona el fitxer de configuració ROSCO");

    if($file) {
        $cui->leave_curses;
        system "$CONF{rosco_load_binary_file} $file";
        sleep(2);
        $cui->reset_curses;
    }
    view_lan_config()
}


sub view_interfaces {

	my $current_y = 3;
	my $current_x = 1;
    my $inc_x = 3;
	my $max_y = 30;

	$cui->delete('win');
	window_create();


    $win->add(
        "LAN",
        "Label",
        -text => "Interfíci WAN > Configuració",
		-bold   => 1,
		-y      => 2,
		-x      => 1,
    );


    my $interfaces = $configuration->{interfaces};

    $win->add(
        "label-input_name", "TextEntry",
        -border => 0,
        -bbg    => $CONF{bg},
        -x => $current_x ,
        -y => $current_y += 2,
        -width => 28,
        -text => "Interfície LAN",
        -focusable => 0,
        -readonly => 1
    );


    my ($index_in_interface) = grep { $interfaces_names[$_] eq $interfaces->{input}->{name} } (0 .. @interfaces_names-1);

    my $field = $win->add(
        "input_name", "Radiobuttonbox",
        -border => 1,
        -bbg    => $CONF{bg},
        -y => $current_y,
        -x => $current_x + 28 ,
        -width => 18,
        -height => 4,
		-selected => $index_in_interface,
		-values => @interfaces_values,
        -labels => $interfaces_labels
    );

    $win->add(
        "label-output_name", "TextEntry",
        -border => 0,
        -bbg    => $CONF{bg},
        -x => $current_x ,
        -y => $current_y += 4,
        -width => 28,
        -text => "Interfície WAN",
        -focusable => 0,
        -readonly => 1
    );

    my ($index_out_interface) = grep { $interfaces_names[$_] eq $interfaces->{output}->{name} } (0 .. @interfaces_names-1);
    #    say first_index { $_ eq 'enp0s8' } @interfaces_names;

    $win->add(
        "output_name", "Radiobuttonbox",
        -border => 1,
        -bbg    => $CONF{bg},
        -y => $current_y,
        -x => $current_x + 28 ,
        -width => 18,
        -height => 4,
		-selected => $index_out_interface,
		-values => @interfaces_values,
        -labels => $interfaces_labels
    );

    #my $field = add_field("output_name", "Name", $interfaces->{output}->{name}, $current_x, $current_y+=1);
    add_field("output_address", "Adreça WAN", $interfaces->{output}->{address}, $current_x, $current_y+=4);
    add_field("output_mask", "Màscara WAN", $interfaces->{output}->{mask}, $current_x, $current_y+=$inc_x);
    add_field("output_DNS1", "DNS primari", $interfaces->{output}->{dns1}, $current_x, $current_y+=$inc_x);
    add_field("output_DNS2", "DNS secundari", $interfaces->{output}->{dns2}, $current_x, $current_y+=$inc_x);

	my $bposition = add_button("GUARDAR", "interfacesSave", \&save_interfaces, $current_x, $current_y + 3, 0);
	add_button("CANCEL·LAR", "interfacesCancel", \&cancel_interfaces, $bposition->{endX}+1, $bposition->{endY}, 0);

	$field->focus();
}

sub save_interfaces {

    $configuration->{interfaces}->{input}->{name} = $win->getobj("input_name")->get();
    $configuration->{interfaces}->{output}->{name} = $win->getobj("output_name")->get();
    $configuration->{interfaces}->{output}->{address} = $win->getobj("output_address")->get();
    $configuration->{interfaces}->{output}->{mask} = $win->getobj("output_mask")->get();
    $configuration->{interfaces}->{output}->{dns1} = $win->getobj("output_DNS1")->get();
    $configuration->{interfaces}->{output}->{dns2} = $win->getobj("output_DNS2")->get();

    my $jsonObject = JSON::XS->new->utf8->pretty(1)->canonical(1)->encode( $configuration );

    # SAVE CONFIG DATA
    open( my $fh, '>', $CONF{config_directory} . "/" . $CONF{config_file});
    print $fh $jsonObject;
    close($fh);

	$cui->dialog(-message => 'Configuració guardada correctament. Recorda que caldrà aplicar els canvis per fer-la efectiva des del menú de configuració.');

    view_menu_home();
}

sub cancel_interfaces {

    my $edited = 0;


    if( $configuration->{interfaces}->{output}->{name} ne $win->getobj("output_name")->get() ) { $edited =1 }
    if( $configuration->{interfaces}->{output}->{address} ne $win->getobj("output_address")->get() ) { $edited =1 }
    if( $configuration->{interfaces}->{output}->{mask} ne $win->getobj("output_mask")->get() ) { $edited =1 }
    if( $configuration->{interfaces}->{output}->{dns1} ne $win->getobj("output_DNS1")->get() ) { $edited =1 }
    if( $configuration->{interfaces}->{output}->{dns2} ne $win->getobj("output_DNS2")->get() ) { $edited =1 }

    if($edited != 0) {
        my $yes = $cui->dialog(
            -message => "S'han produït canvis. Segur que vols cancel·lar i tornar al menú principal?",
            -buttons => [
                { -label => "Sí", -value => 1 },
                { -label => "No", -value => 0 }
            ],
            -bfg     => $CONF{bfg},
            -bbg     => $CONF{bg},
            -bg      => $CONF{bg},
        );

        if ($yes) {
            view_menu_home();
        }
    }else{
        view_menu_home();
    }
}

sub add_button {
	my ($label, $buttonId, $onpress, $current_x, $current_y, $focus) = @_;

	my $labelButton = "[ ".$label." ]";
	my $labelLength = length $labelButton;

	my $button = $win->add(
		"bt-$buttonId", "Buttonbox",
		-buttons => [
		{
			-label => $labelButton,
	 		-onpress => $onpress,
		}],
		-x => $current_x,
		-y => $current_y,
        -width => $labelLength,
       	-bg 	=> "black",
		-fg 	=> "yellow",
	);

	if($focus eq "1") {
		$button->focus();
	}

	return {"endX" => $current_x+$labelLength, "endY" => $current_y };
}

sub add_field {

	my $label_width = 28;
	my $input_width = 18;

    my ($key, $label, $value, $current_x, $current_y) = @_;


    $win->add(
        "label-$key", "TextEntry",
        -border => 0,
        -bbg  	=> $CONF{bg},
        -x => $current_x ,
        -y => $current_y + 1,
        -width => $label_width,
        -text => $label,
        -focusable => 0,
        -readonly => 1
    );
    my $inputText = $win->add(
        "$key", "TextEntry",
        -border => 1,
        -bbg  	=> $CONF{bg},
        -y => $current_y,
        -x => $current_x + $label_width ,
        -text => $value,
        -width => $input_width
    );

	return $inputText;
}

sub view_routers {

	my $current_y = 2;
	my $current_x = 1;
	my $max_y = 30;
	my $label_width = 28;
	my $input_width = 18;
	my $i = 0;

	$cui->delete('win');

	window_create();

    my $firstField;
    my $firstFieldAux;

	foreach my $variable (@{$typology->{variables}}) {
		if($variable->{editable}){
			$win->add(
				"d$i", "TextEntry",
				-border => 0,
				-bbg  	=> $CONF{bg},
				-x => $current_x ,
				-y => $current_y + 1,
				-width => $label_width,
				-text => $variable->{label},
				-focusable => 0,
				-readonly => 1
			);
			$firstFieldAux = $win->add(
				"input-$i", "TextEntry",
				-border => 1,
				-bbg  	=> $CONF{bg},
				-y => $current_y,
				-x => $current_x + $label_width ,
				-text => $variable->{value},
				-width => $input_width
			);
			$current_y += 3;

			if ($current_y > $max_y) {
				$current_y = 2;
				$current_x = 50;
			}
		}
        if($i == 0){ $firstField = $firstFieldAux; }
		$i = $i + 1;
	}


	my $btsave = add_button("GUARDAR", "routersSave", \&save_routers, $current_x, $current_y, 0);
	add_button("CANCEL·LAR", "routersCancel", \&cancel_routers, $btsave->{endX}+1, $btsave->{endY}, 0);

    $firstField->focus();

	# my $button = $win->add(
	# 	"addbutton", "Buttonbox",
	# 	-buttons => [
	# 	{
	# 		-label => "[ APLICA ]",
	# 		-onpress => \&apply_typology
	# 	} ,
	# 	{
	# 		-label => "[ CANCEL·LAR ]",
	# 		-onpress => \&exit
	# 	}
	# 	],
	# 	-y => $current_y + 2,
	# 	-x => $current_x
	# );
	# $button->focus();
}

sub save_routers {

	my $i = 0;

    foreach my $variable (@{$typology->{variables}}) {
		if($variable->{editable}){
			$variable->{value} = $win->getobj("input-$i")->get();
		}
		$i = $i + 1;
	}
    #
    # my $yes = $cui->dialog(
    # 	-message => "Vols aplicar la configuracio " . $typology . "?",
    #     -buttons => [
    #     	{-label => "Sí",-value => 1},
    #     	{-label => "No",-value => 0}
    #     ],
	 #    -bfg  	=> $CONF{bfg},
	 #    -bbg  	=> $CONF{bg},
	 #    -bg 	=> $CONF{bg},
    # );

	my $jsonObject = JSON::XS->new->utf8->pretty(1)->canonical(1)->encode( $configuration );

    # SAVE CONFIG DATA
    open( my $fh, '>', $CONF{config_directory} . "/" . $CONF{config_file});
    print $fh $jsonObject;
    close($fh);

	$cui->dialog(-message => 'Configuració guardada correctament. Recorda que caldrà aplicar els canvis per fer-la efectiva des del menú de configuració.');

    view_menu_home();
}

sub cancel_routers {

	my $i = 0;
    my $edited = 0;

    foreach my $variable (@{$typology->{variables}}) {
		if($variable->{editable}){
			if($variable->{value} ne $win->getobj("input-$i")->get() ){
                $edited = 1;
            }
		}
		$i = $i + 1;
	}

    if($edited != 0) {
        my $yes = $cui->dialog(
            -message => "S'han produït canvis. Segur que vols cancel·lar i tornar al menú principal?",
            -buttons => [
                { -label => "Sí", -value => 1 },
                { -label => "No", -value => 0 }
            ],
            -bfg     => $CONF{bfg},
            -bbg     => $CONF{bg},
            -bg      => $CONF{bg},
        );

        if ($yes) {
            view_menu_home();
        }
    }else{
        view_menu_home();
    }
}

sub apply_changes {
    my $yes = $cui->dialog(
        -message => "Es procedirà a aplicar els canvis de configuració d'interfícies LAN, VLAN's i de routers de sortida (Lustitia).
        \n\nEstàs segur que vols continuar?",
        -buttons => [
            { -label => "No", -value => 0 },
            { -label => "Sí", -value => 1 },
        ],
        -bfg     => $CONF{bfg},
        -bbg     => $CONF{bg},
        -bg      => $CONF{bg},
    );

    if ($yes) {
        # $cui->delete("win");
        # $cui->mainloopExit;

        $cui->leave_curses;
        # system("reset");
        # do_backup("pre-configurador");
        set_variables();
        apply_rosco_configuration();
        execute_commands();
        set_services();
        exit(0);
        # do_backup("post-configurador");
    }
}

sub exit {
	my $yes = $cui->dialog(
   	-message => "Segur que vols sortir del Configurador sense aplicar els canvis?",
        -buttons => [
        	{
        		-label => "Sí",
        		-value => 1
        	},
        	{
        		-label => "No",
        		-value => 0
        	}
        ],
	    -bfg  	=> $CONF{bfg},
	    -bbg  	=> $CONF{bg},
	    -bg 	=> $CONF{bg},
    );

    if ($yes) {
        $cui->mainloopExit;
    }
}

sub execute_commands{
	my $typology_id = shift;

	foreach my $command (@{$typology->{commands}}) {
		system($command);
	}
}

sub do_backup {
    print("Realitzant un backup\n");
	system(
		"/usr/bin/sc-backup",
		shift
	);
    print("Done\n");
}

sub apply_rosco_configuration{
    print("Configurant interfícies LAN i VLAN...\n");
	system("$CONF{rosco_apply_binary_file}");
    print("Configuració LAN i VLAN aplicada\n");
}

sub set_variables {

    print("Lustitia - Assignant variables...\n");

    system(
        "$CONF{working_directory}/$CONF{set_variable}",
        "$CONF{lustitia_file}",
        "ETH_OUT",
        $configuration->{interfaces}->{output}->{name},
        ""
    );

	# TODO: delete the following lines
    #my ($index_interface) = grep { $interfaces_names[$_] eq $configuration->{interfaces}->{output}->{name} } (0 .. @interfaces_names-1);
    ## my $index_in = abs $index_interface-1;

    system(
        "$CONF{working_directory}/$CONF{set_variable}",
        "$CONF{lustitia_file}",
        "ETH_IN",
        $configuration->{interfaces}->{input}->{name},
        ""
	);

	my $router_1_enabled = 0;

	foreach my $variable (@{$typology->{variables}}) {

		system(
			"$CONF{working_directory}/$CONF{set_variable}",
			$variable->{file},
			$variable->{name},
			$variable->{value},
			$variable->{wrapper}
		);

		if( $variable->{name} eq "ROUTER_1_ENABLE" && $variable->{value} eq "1"){
			$router_1_enabled = 1;
		}
	}

	my $n_routers = $router_1_enabled == 1 ? 2 : 1;

	system(
			"$CONF{working_directory}/$CONF{set_variable}",
			"$CONF{lustitia_file}",
			"N_ROUTERS",
			$n_routers,
			""
		);

        system(
                        "$CONF{working_directory}/$CONF{set_variable}",
                        "$CONF{dhcp_server_file}",
                        "INTERFACESv4",
                        $configuration->{interfaces}->{input}->{name},
                        "\""
                );

    print("Lustitia - Configuració establerta\n");
}

sub set_services {

    print("Reiniciant serveis...\n");
	foreach my $service (@{$typology->{services}}) {
		if ($service->{value}) {
			system("service $service->{name} restart");
		} else {
			system("service $service->{name} stop");
		}
	}
    print("Serveis reiniciats\n");
}

#view_lan_rosco_filebrowser();
#edit_typology();
view_menu_home();
$cui->set_binding( \&exit, CUI_ESCAPE);
$cui->mainloop;
