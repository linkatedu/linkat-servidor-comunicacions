#!/bin/bash

DESTI=/var/lib/squidguard/db/blacklists/
LOGS=/var/log/mirroring_blacklists.log

function update_blacklists {
        /usr/bin/rsync -av --log-file $LOGS rsync://download-linkat.xtec.cat/blacklists $DESTI
	/usr/share/linkat-servidor-comunicacions/generate_squidguard_db.sh
}


if [ "$0" = "$BASH_SOURCE" ]; then
       echo "Start updating black lists"
       update_blacklists
fi


