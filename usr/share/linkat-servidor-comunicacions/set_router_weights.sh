#!/bin/bash

. /etc/lustitia.conf

if [ $ROUTER_0_ENABLE -eq 0 ]; then
	if [ $ROUTER_1_ENABLE -eq 0 ]; then
		echo "ALL DISABLE"
		exit
	else
		POND_1=1
	fi
else
	if [ $ROUTER_1_ENABLE -eq 0 ]; then
		POND_0=1
	else
		NUM1=$ROUTER_0_DOWNLOAD
		NUM2=$ROUTER_1_DOWNLOAD

		if [ $NUM1 -lt $NUM2 ]
		then
			POND_1=$((NUM2/NUM1)) 
			POND_0=1
		else
			POND_0=$((NUM1/NUM2))
			POND_1=1
		fi

	fi
fi

DEST_FILE="/etc/lustitia.conf"
sed -i "s/\(ROUTER_0_WEIGHT=\).*/\1$POND_0/" $DEST_FILE
sed -i "s/\(ROUTER_1_WEIGHT=\).*/\1$POND_1/" $DEST_FILE
