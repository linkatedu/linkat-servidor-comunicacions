#!/bin/bash

. /etc/lustitia.conf


SQUID_CONF=/etc/squid/squid.conf

if [ $ROUTER_1_ENABLE == 0 ]
then
	MAX_DOWNLOAD=$ROUTER_0_DOWNLOAD
else
	MAX_DOWNLOAD=`expr $ROUTER_0_DOWNLOAD + $ROUTER_1_DOWNLOAD`
fi


DELAY_POOL_DOWNLOAD=`expr $MAX_DOWNLOAD "*" $DELAY_POOL_PERCENTAGE`
DELAY_POOL_DOWNLOAD=`expr $DELAY_POOL_DOWNLOAD "/" 100`
DELAY_POOL_DOWNLOAD=`expr $DELAY_POOL_DOWNLOAD "*" 1024`
DELAY_POOL_DOWNLOAD=`expr $DELAY_POOL_DOWNLOAD "*" 1024` # Because from September 2018 we're using Mbits instead of Kbits
DELAY_POOL_DOWNLOAD=`expr $DELAY_POOL_DOWNLOAD "/" 8`

TOTAL_PERMITTED=`expr $DELAY_POOL_DOWNLOAD "*" 4`


DP_VALUE="delay_parameters 1 $DELAY_POOL_DOWNLOAD\/$TOTAL_PERMITTED"


# Replace if exists. Add if not exists
grep -q '^delay_parameters' $SQUID_CONF && sed -i "s/^delay_parameters 1.*/$DP_VALUE/" $SQUID_CONF || echo $DP_VALUE >> $SQUID_CONF


