#!/usr/bin/python3
import re
import pprint
import argparse
import ipaddress
import iptc
import subprocess
import io
from yaml import load, dump
import json
import time

ROSCO_CONFIG_FILE = "/etc/linkat-servidor-comunicacions/rosco.json"
ROSCO_ALL_CONFIG_FILE = "/etc/linkat-servidor-comunicacions/rosco_all.json"
NETPLAN_CONFIG_FILE = "/etc/netplan/50-cloud-init.yaml"
DHCP_CONFIG_FOLDER = "/etc/dhcp"
DHCP_CONFIG_FILE = DHCP_CONFIG_FOLDER + "/dhcpd.conf"
DHCP_SUBNETS_CONFIG_FOLDER = DHCP_CONFIG_FOLDER+"/subnets"
VLAN_SC_IP=9

# data sctructure for saving data parsed
data = { "vlans":{},
        "dhcps":{},
        "access-lists":{}}

# data sctructure for saving data parsed
finalData = { "vlans":{},
        "dhcps":{},
        "access-lists":{}}

def parse(filepath):

    with open(filepath, 'r') as file:
        matcher = ParserRegularExpressions()
        line = next(file)
        while line:

            # VLAN lines type "vlan XY"
            if matcher.vlan_defintion_start(line):
                id = matcher.vlan_defintion_start(line).group(1)
                vlan  = getVlanData(id)

                while (not matcher.end(line)):
                    if matcher.vlan_name(line):
                        vlan["name"] = matcher.vlan_name(line).group(1).lower()
                    line = next(file, None)

            # VLAN lines type "interface VlanXY"
            elif matcher.vlan_interface_start(line):
                id = matcher.vlan_interface_start(line).group(1)
                vlan  = getVlanData(id)

                while (not matcher.end(line)):
                    if matcher.vlan_description(line):
                        vlan["description"] = matcher.vlan_description(line).group(1)
                    elif matcher.vlan_address(line):
                        vlan["gw"] = matcher.vlan_address(line).group(1)
                        vlan["mask"] = matcher.vlan_address(line).group(2)

                        network = ipaddress.IPv4Network('%s/%s' % (vlan["gw"],vlan["mask"]), strict=False)
                        vlan["network"] = str(network.network_address)
                        vlan["address"] = str(network.network_address+VLAN_SC_IP)
                    elif matcher.vlan_accessgroup(line):
                        vlan["accessgroup"] = []
                        vlan["accessgroup"].append({ "group": matcher.vlan_accessgroup(line).group(1), "dest": matcher.vlan_accessgroup(line).group(2) })
                    line = next(file, None)

            #DHCP VLAN lines type "ip dhcp pool <VlanName>"
            elif matcher.dhcp_pool(line):
                id = matcher.dhcp_pool(line).group(1)
                dhcp  = getVlanDHCP(id)

                while (not matcher.end(line)):
                    if matcher.dhcp_pool_network(line):
                        dhcp["network"] = matcher.dhcp_pool_network(line).group(1)
                        dhcp["mask"] = matcher.dhcp_pool_network(line).group(2)
                    elif matcher.dhcp_pool_gw(line):
                        dhcp["gw"] = matcher.dhcp_pool_gw(line).group(1)
                    elif matcher.dhcp_pool_dns_server(line):
                        dhcp["dns"] = []
                        dhcp["dns"].append(matcher.dhcp_pool_dns_server(line).group(1))
                        dhcp["dns"].append(matcher.dhcp_pool_dns_server(line).group(2))
                    line = next(file, None)

            # ACCESS LIST lines type "access-list 69 deny|permit ip [mask]"
            elif matcher.access_list_A(line):
                id = matcher.access_list_A(line).group(1)
                type = matcher.access_list_A(line).group(2)
                target = matcher.access_list_A(line).group(3)
                mask = matcher.access_list_A(line).group(4)

                accessList = getAccessList(id)
                accessList.append({"type": type, "target":target, "mask": mask})

            # ACCESS LIST lines type "access-list 120 deny|permit protocol [any|ip mask] [any|ip mask]"
            elif matcher.access_list_B(line):
                id      = matcher.access_list_B(line).group(1)
                type    = matcher.access_list_B(line).group(2)
                protocol = matcher.access_list_B(line).group(3)
                source  = matcher.access_list_B(line).group(4)
                target  = matcher.access_list_B(line).group(5)

                accessList = getAccessList(id)
                accessList.append({"type": type, "protocol": protocol, "source":source, "target":target, "mask": mask})

            line = next(file, None)
    return data

def getVlanData(id):
    global data

    if id in data['vlans']:
        return data['vlans'][id]
    else:
        vlan = {"id": id}
        data["vlans"][id] = vlan
        return vlan

def getVlanDHCP(idName):
    global data

    idName = idName.lower()

    if idName in data['dhcps']:
        return data['dhcps'][idName]
    else:
        dhcp = {"name": idName}
        data["dhcps"][idName] = dhcp
        return dhcp

def getAccessList(id):
    global data

    if not id in data['access-lists']:
        data["access-lists"][id] = []

    return data['access-lists'][id]

class ParserRegularExpressions:

    _end = re.compile('^!\n')

    #VLAN patterns
    _vlan_definition_start = re.compile('^vlan ([0-9]+)\n')
    _vlan_interface_start = re.compile('^interface Vlan(.*)\n')
    _vlan_name = re.compile('^\sname (.*)\n')
    _vlan_description = re.compile('^\sdescription (.*)\n')
    _vlan_address = re.compile('^\sip address (.*) (.*)\n')
    _vlan_accessgroup = re.compile('^\sip access-group (.*) (in|out)\n')

    #DHCP VLAN's patterns
    _dhcp_pool = re.compile('^ip dhcp pool (.*)\n')
    _dhcp_pool_network = re.compile('^\snetwork\s([0-9.]*)\s([0-9.]*)\n')
    _dhcp_pool_gw = re.compile('^\sdefault-router\s([0-9.]*)\s*\n')
    _dhcp_pool_dns_server = re.compile('^\sdns-server\s([0-9.]*)\s([0-9.]*)\s*\n')

    #ACCESS LIST patterns
    _access_list_type_A = re.compile('^access-list ([0-9]+) (permit|deny)\s+([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})(\s[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})?\n')
    # _access_list_type_B = re.compile('^access-list ([0-9]+) (permit|deny)\s+ip ([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}|any)+ ([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}|any)(\s[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})?\n')
    _access_list_type_B = re.compile('^access-list ([0-9]+) (permit|deny)\s+(\w+) (any|[a-z0-9.]+ [0-9.]+) (any|[0-9.]+ [0-9.]+)?\n')


    def end(self, line):
        return self._end.match(line)

    def vlan_defintion_start(self, line):
        return self._vlan_definition_start.match(line)

    def vlan_interface_start(self, line):
        return self._vlan_interface_start.match(line)

    def vlan_name(self, line):
        return self._vlan_name.match(line)

    def vlan_description(self, line):
        return self._vlan_description.match(line)

    def vlan_address(self, line):
        return self._vlan_address.match(line)

    def vlan_accessgroup(self, line):
        return self._vlan_accessgroup.match(line)

    def access_list_A(self, line):
        return self._access_list_type_A.match(line)

    def access_list_B(self, line):
        return self._access_list_type_B.match(line)

    def dhcp_pool(self, line):
        return self._dhcp_pool.match(line)

    def dhcp_pool_network(self, line):
        return self._dhcp_pool_network.match(line)

    def dhcp_pool_gw(self, line):
        return self._dhcp_pool_gw.match(line)

    def dhcp_pool_dns_server(self, line):
        return self._dhcp_pool_dns_server.match(line)

def netmask_to_cidr(netmask):
    '''
    :param netmask: netmask ip addr (eg: 255.255.255.0)
    :return: equivalent cidr number to given netmask ip (eg: 24)
    '''
    return sum([bin(int(x)).count('1') for x in netmask.split('.')])

def iptables_generate_rules(access_group, vlan):

    if access_group in data["access-lists"]:
        for ruleRosco in data["access-lists"][access_group]:
            chain = iptc.Chain(iptc.Table(iptc.Table.FILTER), "INPUT")

            rule = iptc.Rule()
            rule.in_interface = vlan
            rule.protocol = ruleRosco["protocol"]

            if ruleRosco["source"].startswith("host"):
                rule.src = ruleRosco["source"].split(" ")[1]
            elif ruleRosco["source"] != "any":
                rule.src = ruleRosco["source"].replace(" ","/")


            if ruleRosco["target"] != "any":
                rule.dst = ruleRosco["target"].replace(" ","/")

            if ruleRosco["type"] == "permit":
                target = rule.create_target("ACCEPT")
            if ruleRosco["type"] == "deny":
                target = rule.create_target("DROP")

            chain.insert_rule(rule)


def dhcp_generate_subnets(dhcp_name):

    if(dhcp_name in data["dhcps"]):
        last_ip = ipaddress.IPv4Network('%s/%s' % (data["dhcps"][dhcp_name]["network"], data["dhcps"][dhcp_name]["mask"]))[-1]
        first_ip = ipaddress.ip_address(data["dhcps"][dhcp_name]["network"])+256
        dns = ", ".join(data["dhcps"][dhcp_name]["dns"])

        subnet = """
        subnet %s netmask %s {
            default-lease-time 86400;
            max-lease-time 86400;
            option routers 172.58.0.9;
            option subnet-mask %s;
            option domain-name-servers %s;
            range %s %s;

        }
        """ % ( data["dhcps"][dhcp_name]["network"], data["dhcps"][dhcp_name]["mask"],
                data["dhcps"][dhcp_name]["mask"], dns, first_ip, last_ip)

        stream = open(DHCP_CONFIG_FOLDER + "/%s.conf" % dhcp_name, 'w')
        stream.write(subnet)
        stream.close()
        # Check if import subnet doesn't exists

        with open(DHCP_CONFIG_FILE, "a") as myfile:
            myfile.write('include "%s/%s.conf";\n' % (DHCP_SUBNETS_CONFIG_FOLDER, dhcp_name))



if __name__ == '__main__':

    #CLI arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("file", help="Specifies de r0sc0 configuration file path.")
    args = parser.parse_args()

    #Main parsing
    filepath = args.file
    print("Llegint el fitxer de ROSCO...")
    data = parse(filepath)
    # pp = pprint.PrettyPrinter(indent=4)
    print("Dades de ROSCO parsejades.")

    print("Filtrant dades...")
    for id, vlan in data["vlans"].items():

        id = int(id)

        # Restrict to Educational VLAN's
        # Requirement from M.Gurri: Educació (20 - 29) i Educació_Wifi (71-74)
        if (20 <= id <= 29 or 70 <= id <= 74):
            finalData["vlans"][id] = vlan

            if "accessgroup" in vlan:
                for group in vlan["accessgroup"]:
                    if group["group"] in data["access-lists"]:
                        finalData["access-lists"][group["group"]] = data["access-lists"][group["group"]]

            if (vlan['name'] in data["dhcps"]):
                finalData["dhcps"][vlan['name']] = data["dhcps"][vlan['name']]


    print("Dades filtrades")
    print("Guardant dades...")

    with open(ROSCO_CONFIG_FILE, 'w') as outfile:
        json.dump(finalData, outfile, indent=4)

    print("Dades filtrades de ROSCO guardades a %s" % ROSCO_CONFIG_FILE)

    with open(ROSCO_ALL_CONFIG_FILE, 'w') as outfile:
        json.dump(data, outfile, indent=4)

    print("Dades completes de ROSCO guardades a %s" % ROSCO_ALL_CONFIG_FILE)

    time.sleep(2)