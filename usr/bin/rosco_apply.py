#!/usr/bin/python3
import re
import pprint
import argparse
import ipaddress
import iptc
import subprocess
import io
from yaml import load, dump
import json
import time

ROSCO_CONFIG_FILE = "/etc/linkat-servidor-comunicacions/rosco.json"
SC_CONFIG_FILE = "/etc/linkat-servidor-comunicacions/config.json"
NETPLAN_CONFIG_FILE = "/etc/netplan/50-cloud-init.yaml"
DHCP_CONFIG_FOLDER = "/etc/dhcp"
DHCP_CONFIG_FILE = DHCP_CONFIG_FOLDER + "/dhcpd.conf"
DHCP_SUBNETS_CONFIG_FOLDER = DHCP_CONFIG_FOLDER+"/subnets"
SQUID_CONFIG_FILE = "/etc/squid/squid.conf"


def netmask_to_cidr(netmask):
    '''
    :param netmask: netmask ip addr (eg: 255.255.255.0)
    :return: equivalent cidr number to given netmask ip (eg: 24)
    '''
    return sum([bin(int(x)).count('1') for x in netmask.split('.')])

def iptables_generate_rules(access_group, vlan):

    if access_group in data["access-lists"]:
        for ruleRosco in data["access-lists"][access_group]:
            chain = iptc.Chain(iptc.Table(iptc.Table.FILTER), "INPUT")

            rule = iptc.Rule()
            rule.in_interface = vlan
            rule.protocol = ruleRosco["protocol"]

            if ruleRosco["source"].startswith("host"):
                rule.src = ruleRosco["source"].split(" ")[1]
            elif ruleRosco["source"] != "any":
                rule.src = ruleRosco["source"].replace(" ","/")


            if ruleRosco["target"] != "any":
                rule.dst = ruleRosco["target"].replace(" ","/")

            if ruleRosco["type"] == "permit":
                target = rule.create_target("ACCEPT")
            if ruleRosco["type"] == "deny":
                target = rule.create_target("DROP")

            chain.insert_rule(rule)


def dhcp_generate_subnets(dhcp_name):

    if(dhcp_name in data["dhcps"]):
        last_ip = ipaddress.IPv4Network('%s/%s' % (data["dhcps"][dhcp_name]["network"], data["dhcps"][dhcp_name]["mask"]))[-1]
        first_ip = ipaddress.ip_address(data["dhcps"][dhcp_name]["network"])+30
        gw_ip = ipaddress.ip_address(data["dhcps"][dhcp_name]["network"])+9
        dns = ", ".join(data["dhcps"][dhcp_name]["dns"])

        subnet = """
        subnet %s netmask %s {
            default-lease-time 86400;
            max-lease-time 86400;
            option routers %s;
            option subnet-mask %s;
            option domain-name-servers %s;
            range %s %s;

        }
        """ % ( data["dhcps"][dhcp_name]["network"], data["dhcps"][dhcp_name]["mask"],
                gw_ip,
                data["dhcps"][dhcp_name]["mask"], dns, first_ip, last_ip)

        stream = open(DHCP_SUBNETS_CONFIG_FOLDER + "/%s.conf" % dhcp_name, 'w')
        stream.write(subnet)
        stream.close()
        # Check if import subnet doesn't exists

        with open(DHCP_CONFIG_FILE, "a") as myfile:
            myfile.write('include "%s/%s.conf";\n' % (DHCP_SUBNETS_CONFIG_FOLDER, dhcp_name))

def squid_add_lan(vlan):

    netmask = netmask_to_cidr(vlan["mask"])
    squid_line = "acl lan src %s/%s\n" % (vlan["network"], netmask)

    with open(SQUID_CONFIG_FILE, 'r+') as file:
        content = file.read()
        file.seek(0,0)
        file.write(squid_line + content)




if __name__ == '__main__':

    json_data = open(ROSCO_CONFIG_FILE).read()
    data = json.loads(json_data)

    json_data = open(SC_CONFIG_FILE).read()
    sc_config = json.loads(json_data)

    input_interface = sc_config["interfaces"]["input"];
    output_interface = sc_config["interfaces"]["output"]
    gateway = ""

    for variable in sc_config["wan"]["variables"]:
        if variable["name"] == "ROUTER_0_IP":
            gateway = variable["value"]
            break

    #Netplan configuration
    stream = open(NETPLAN_CONFIG_FILE, 'r')
    netplan = load(stream)

    netplan["network"]["vlans"] =  {} #empty vlans config in netplan
    netplan["network"]["ethernets"] =  {} #empty ethernets config in netplan

    # Disable autcommit iptables rules
    table = iptc.Table(iptc.Table.FILTER)
    table.autocommit = False

    # Delete all imports of subnets in DHCPD
    stream = open(DHCP_CONFIG_FILE, 'r')
    dhcpd = stream.read()
    new_dhcpd = re.sub("include \"%s/.+\.conf\";\n" % DHCP_SUBNETS_CONFIG_FOLDER, "", dhcpd)

    stream = open(DHCP_CONFIG_FILE, 'w')
    stream.write(new_dhcpd)
    stream.close()

    # Delete all 'acl lan src' from squid
    stream = open(SQUID_CONFIG_FILE, "r")
    squid = stream.read()
    new_squid = re.sub("acl lan src .+\n", "", squid)

    stream = open(SQUID_CONFIG_FILE, 'w')
    stream.write(new_squid)
    stream.close()


    #Configuring Netplan interfaces
    #LAN
    netplan["network"]["ethernets"][input_interface["name"]] = {}
    #WAN
    netplan["network"]["ethernets"][output_interface["name"]] = {}
    netplan["network"]["ethernets"][output_interface["name"]]["addresses"] = ["%s/%d" % (output_interface["address"],netmask_to_cidr(output_interface["mask"]))]
    netplan["network"]["ethernets"][output_interface["name"]]["nameservers"] = {"addresses": [output_interface["dns1"], output_interface["dns2"]] }
    netplan["network"]["ethernets"][output_interface["name"]]["gateway4"] = gateway

    #Search in all obtained VLAN's
    for id, vlan in data["vlans"].items():

        id = int(id)

        # Restrict to Educational VLAN's
        # Requirement from M.Gurri: Educació (20 - 29) i Educació_Wifi (71-74)
        if( 20 <= id <= 29  or 70 <= id <= 74  ):

            # Get netmask in /xx (CIDR) format
            mask = netmask_to_cidr(vlan["mask"])

            # Add vlan item configuration in netplan
            netplan["network"]["vlans"]["vlan%d" % id] = {"id": id,
                                                        "link": input_interface["name"],
                                                        "addresses": ["%s/%d" % (vlan["address"], mask)],
                                                        "routes": [{"to":"10.0.0.0/8", "via":"192.168.0.1"}]}


            # iptables application for vlan
            if "accessgroup" in vlan:
                for group in vlan["accessgroup"]:
                    iptables_generate_rules(group["group"], vlan['name'])

            # DHCP subnet definition for vlan
            dhcp_generate_subnets(vlan['name'])
            squid_add_lan(vlan)


    #Static route
    for key,interface in netplan["network"]["ethernets"].items():
        netplan["network"]["ethernets"][key]["routes"] = []
        netplan["network"]["ethernets"][key]["routes"].append({"to":"10.0.0.0/8", "via":"192.168.0.1"})


    # Saving netplan configuration
    output = dump(netplan, default_flow_style=False)
    stream = open(NETPLAN_CONFIG_FILE, 'w')
    stream.write(output)
    stream.close()


    # Netplan Apply
    subprocess.run(["ip","link","set","dev", input_interface["name"], "down"])
    subprocess.run(["netplan", "apply"])
    time.sleep(5)
    subprocess.run(["ip","link","set","dev", input_interface["name"], "up"])

    # DHCP restart
    subprocess.run(["service", "isc-dhcp-server", "restart"])

    # IPTABLES Activation
    table.commit()
    table.autocommit = True

    # pp.pprint(data)

