#!/bin/bash


. /usr/share/linkat-servidor-comunicacions/blacklists_update.sh
_DATE=`date +'%k'`

if [ $_DATE -gt "0" ] && [ $_DATE -lt "8" ] ;then
	RANGE=10800
        rand=$RANDOM;let "rand %= $RANGE"
        echo "Sleeping $rand seconds before updating the blacklists"
        sleep $rand;
        update_blacklists
else
	echo "SKIPPING - not in range hour"
fi
